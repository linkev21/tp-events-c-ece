#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "event.h"

/**
 * 3.1
 */
int countCaracters(Event* anchor)
{
    int count = 0;

    //Si liste non vide
    if(anchor != NULL)
    {
        Event* current = anchor;

        while(current != NULL)
        {
            count += strlen(current->title);

            current = current->nextEvent;
        }
    }

    return count;
}

/**
 * 3.2
 */
void writeEventInto(FILE* file, Event* event)
{
    fprintf(file, "Identifiant : %d\n", event->id);
    fprintf(file, "Titre : %s\n", event->title);
    fprintf(file, "Date : %d\n\n", event->date);
}

void printCollectionTo(Event* anchor, char* filename)
{
    FILE* file = fopen(filename, "w+");

    //si l'ouverture du fichier a réussie
    if(file != NULL && anchor != NULL)
    {
        Event* current = anchor;

        while(current != NULL)
        {
            writeEventInto(file, current);

            current = current->nextEvent;
        }

        fprintf(file, "Nombre de caracteres total : %d\n", countCaracters(anchor));

        fclose(file);
    }
}

