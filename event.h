#ifndef EVENTS_EVENT_H
#define EVENTS_EVENT_H

/**
 * 1.1
 */
typedef struct Event Event;
struct Event
{
    char* title;
    unsigned int id;
    unsigned int date;

    Event* nextEvent;
};

/**
 * 1.2
 */
Event* initEvent();

/**
 * 1.3
 */
void displayEvent(Event* event);

#endif //EVENTS_EVENT_H


