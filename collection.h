#ifndef EVENTS_COLLECTION_H
#define EVENTS_COLLECTION_H

#include "event.h"

/**
 * 2.1
 */

void addNewEvent(Event* anchor, Event* newEvent);

/**
 * 2.2
 */
void displayEvents(Event* anchor);

#endif //EVENTS_COLLECTION_H
