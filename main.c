#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "event.h"
#include "collection.h"
#include "file.h"

int main() {
    srand(time(NULL));

    int collectionSize = 0;
    Event* anchor = NULL;

    printf("Nombre d'évenement à inserer ? ");
    scanf("%d", &collectionSize);

    anchor = initEvent();

    for(int i = 1; i < collectionSize; i++)
    {
        Event* event = initEvent();
        addNewEvent(anchor, event);
    }

    displayEvents(anchor);

    printf("Nombre de caractères total : %d", countCaracters(anchor));

    printCollectionTo(anchor, "events.txt");

    return 0;
}