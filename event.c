#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "event.h"

/**
 * 1.2
 */
Event* initEvent()
{
    Event* event = malloc(sizeof(*event));
    char title[256];

    if (event == NULL) {
        printf("Problème d'initialisation du block");
        exit(EXIT_FAILURE);
    }

    event->id = rand() % 1000 + 1;

    printf("Titre de l'événement : ");
    scanf("%s", title);

    event->title = (char*) malloc(sizeof(char) * (strlen(title) + 1));
    strcpy(event->title, title);

    do{
        printf("Saissisez un nombre compris entre 1 et 31 : \n");
        scanf("%d", &(event->date));
    }while(event->date < 1 || event->date > 31);

    event->nextEvent = NULL;

    return event;
}

/**
 * 1.3
 */
void displayEvent(Event* event)
{
    printf("Identifiant : %d\n", event->id);
    printf("Titre : %s\n", event->title);
    printf("Date : %d\n\n", event->date);
}
