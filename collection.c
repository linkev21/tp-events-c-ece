#include <stdio.h>
#include <string.h>
#include "event.h"

/**
 * 2.1
 * Aout en fin de liste
 */

void addNewEvent(Event* anchor, Event* newEvent)
{
    //Si liste non vide
    if(anchor != NULL)
    {
        Event* current = anchor;

        while(current->nextEvent != NULL)
        {
            current = current->nextEvent;
        }

        current->nextEvent = newEvent;
    }
}

/**
 * 2.2
 */
void displayEvents(Event* anchor)
{
    //Si liste vide
    if(anchor == NULL)
    {
        printf("Liste vide");
    }else
    {
        Event* current = anchor;

        while(current != NULL)
        {
            displayEvent(current);

            current = current->nextEvent;
        }
    }
}