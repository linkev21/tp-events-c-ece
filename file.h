#ifndef EVENTS_FILE_H
#define EVENTS_FILE_H

#include "event.h"

/**
 * 3.1
 */
int countCaracters(Event* anchor);

/**
 * 3.2
 */
void writeEventInto(FILE* file, Event* event);
void printCollectionTo(Event* anchor, char* file);

#endif //EVENTS_FILE_H
